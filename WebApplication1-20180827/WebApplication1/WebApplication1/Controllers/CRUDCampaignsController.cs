﻿using WebApplication1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Data;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;


namespace WebApplication1.Controllers
{

    public class CRUDCampaignsController : Controller
    {
        private SponsorDBContext db = new SponsorDBContext();

        // GET: CRUDCampaigns
        public ActionResult Index()
        {
            var campaigns = from d in db.Campaigns
                            where d.Status == "on-going"
                            select d;
            return View(campaigns);
        }

        public ActionResult Index2()
        {
            var campaigns = from d in db.Campaigns
                            where d.Status == "completed"
                            select d;
            return View(campaigns);
        }

        // GET: CRUDCampaigns/Details/5
        public ActionResult Details(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Campaign campaign = db.Campaigns.Find(id);
            if(campaign == null)
            {
                return HttpNotFound();
            }
            return View(campaign);
        }

        // GET: CRUDCampaigns/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: CRUDCampaigns/Create
        //[Authorize]
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {

            var name = collection["Name"];
            var description = collection["Description"];
            var begin_date = DateTime.Parse(collection["Begin_date"]);
            var end_date = DateTime.Parse(collection["End_date"]);
            var status = collection["Status"].ToString();
            var type = collection["Type"].ToString();

            var campaign = new Campaign();
            campaign.Name = name;
            campaign.Description = description;
            campaign.Type = type;
            campaign.Begin_date = begin_date;
            campaign.End_date = end_date;
            campaign.Status = status;
            campaign.owner_id = User.Identity.GetUserId();

            if (ModelState.IsValid)
            {
                db.Campaigns.Add(campaign);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View();
        }

        

        /*private List<Campaign> GetCampaigns ()
        {
            var dbContext = new SponsorDBContext();

            var campaigns = dbContext.Campaigns.ToList();

            return campaigns;
        }*/
    }
}
