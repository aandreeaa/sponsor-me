﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class Campaign
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Display(Name = "Begin Date")]
        public DateTime Begin_date { get; set; }

        [Display(Name = "End Date")]
        public DateTime? End_date { get; set; }

        public DateTime? Finished { get; set; }

        public string Status { get; set; }

        public string Type { get; set; }

        public string owner_id { get; set; }
    }

    public class Category
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

    public class CampaignCategory
    {
        public int Id { get; set; }

        public int campaign_id { get; set; }

        public int category_id { get; set; }
    }

    public class Entity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string Number { get; set; }

        public string Type { get; set; }

        public string Description { get; set; }

        public string ContactPerson { get; set; }

        public string user_id { get; set; }
    }

    public class EntityCampaign
    {
        public int Id { get; set; }

        public int campaign_id { get; set; }

        public int cpplicant_id { get; set; }
    }

    public class Link
    {
        public int Id { get; set; }

        public string Url { get; set; }

        public int entity_id { get; set; }
    }

    public class SponsorDBContext : DbContext
    {
        public DbSet<Campaign> Campaigns { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<CampaignCategory> CampaignCategories { get; set; }

        public DbSet<Entity> Entities { get; set; }

        public DbSet <EntityCampaign> EntityCampaigns { get; set; }

        public DbSet <Link> Links { get; set; }
    }

    public class DefaultConnection : DbContext
    {

    }

}